/**
 * Created by t430s on 7/11/2015.
 */

(function(angular){
  angular.module('ap.services').factory('messagingService',
    ['ajaxService', '$rootScope', '$location', '$log', 'storageService', '$q',
      function (ajaxService, $rootScope, $location, $log, storageService, $q) {

        var handlers = {};

        function publish (e, args) {
          if(!handlers[e] || !Array.isArray(handlers[e])) {
            return;
          }
          handlers[e].forEach(function(v, i, arr) {
            v.apply(null, [args]);
          });
          return;
        }

        function subscribe(e, h) {
          if(!e || !h) {
            return false;
          }
          if(!handlers[e]) {
            handlers[e] = [];
          }

          handlers[e].push(h);

          //return [e, h];
          //Build and return a cancel function.
          return function() {
            return unsubscribe([e, h]);
          }
        }

        function unsubscribe(h) {
          //Heck if h is a function. and call it.
          if(typeof h === 'function') {
            return h();
          }

          //Check if handle is an array.
          if(!Array.isArray(h)) {
            return false;
          }

          if(handlers[h[0]] && Array.isArray(handlers[h[0]])) {
            handlers[h[0]].forEach(function(v, i, arr) {
              //Check if the handler function match.
              if(v === h[1])
              //Remove the handler from the array.
              handlers[h[0]].splice(i, 1);
            });
          }

          //Todo: Check if there are no more handlers set to the specific event, and remove the object key.
          return true;
        }

        return {
          publish: publish,
          emit: publish,
          subscribe: subscribe,
          on: subscribe,
          unsubscribe: unsubscribe
        }
      }])
})(angular);
