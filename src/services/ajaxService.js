/**
 * Created by t430s on 7/10/2015.
 */
/*
 * Customized Ajax service with special registry object to store data used across different templates.
 * This is to prevent ajax calls for the same data at different parts of the application.
 */

(function () {
  angular.module('ap.services').factory('ajaxService',
    ['$http', '$q', '$log',
      function ($http, $q, $log) {
        function buildQuery(args) {
          var q = '?';
          for( var key in args) {
            if(args.hasOwnProperty(key)) {
              var val = args[key];
              // If Key is an object, create an container for the current key.
              //Todo: Do this recursively!! not a stupid if pyramid.
              if(typeof val === 'object') {
                for( var name in val) {
                  if(val.hasOwnProperty(name)) {
                    q += key +'[' + name + ']=' + val[name] + '&';
                  }
                }
              } else {
                //console.log('instanceof', instanceof val);
                q += key + '='+ val + '&';
              }

            }
          }

          return q;
        }

        var registry = {
          url: {}
        };

        var get = function (url, args) {
          var args = args || {};

          //check when the last time the api was called and determine if the cashed data stale.
          var noCache = args.noCache || false;

          //Check if params were supplied and build the unique URL.
          //This should be before the registry check so that urls with different query parameters do not register
          //as being the same request query.
          if(args.params) {
            url += buildQuery(args.params);
          }

          if (!noCache && registry.url[url]) {
            //check if the resolved flag is set, if true, return a resolved promise
            return registry.url[url].resolved ?
              $q.resolve(registry.url[url].data) :
              registry.url[url].data;

            //if false return a reference to the current promise.
          }



          var promise = $http.get(url)
            .then(function (data) {
              registry.url[url] = {
                data: data,
                resolved: true
              };
              return data;
            });

          //store the promise in the registry and set a flag to identify it as a promise.
          registry.url[url] = {
            data: promise,
            resolved: false
          };

          return promise;
        };

        var post = function (url, data) {
          //create a custom post object to submit the correct header for PHP to be able to deserialize the data.
          return $http({
            method: 'POST',
            url: url,
            data: data
          }).then(function (data) {

            return data;

          }).catch(function (err) {
            //use the messenger service the log the errors.
            console.log('AjaxService.post()-err');
            console.log(err);
            //Pass the error object on.
            return(err);

          });
        };

        var put = function (url, data) {
          //create a custom post object to submit the correct header for PHP to be able to deserialize the data.
          return $http({
            method: 'PUT',
            url: url,
            data: data
          }).then(function (data) {

            return data;

          }).catch(function (err) {
            //use the messenger service the log the errors.
            console.log('AjaxService.put()-err');
            console.log(err);
            //Pass the error object on.
            return(err);

          });
        };

        var _delete = function(url, data, config) {
          return $http({
            url: url,
            method: 'DELETE',
            data: data,
            headers: {"Content-Type": "application/json;charset=utf-8"}
          }).then(function(data) {
            return data;
          }).catch(function(err){
            console.log('AjaxService.post() - error', err);
          })
        }

        var api = {
          get: get,
          post: post,
          put: $http.put,
          delete: _delete
        };
        return api;
      }])
}());
