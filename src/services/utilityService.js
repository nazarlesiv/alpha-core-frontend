/**
 * Created by Nazar Lesiv on 8/11/2015.
 */
(function () {
  angular.module('ap.services').factory('utilityService',
    [function () {

      function isValidUrl(url) {
        var pattern = new RegExp('^(https?:\/\/)?'+ // protocol
          '((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|'+ // domain name
          '((\d{1,3}\.){3}\d{1,3}))'+ // OR ip (v4) address
          '(\:\d+)?(\/[-a-z\d%_.~+]*)*'+ // port and path
          '(\?[;&a-z\d%_.~+=-]*)?'+ // query string
          '(\#[-a-z\d_]*)?$','i'); // fragment locater
        if(!pattern.test(str)) {
          alert("Please enter a valid URL.");
          return false;
        } else {
          return true;
        }
      }

      //Method that will create an anchor tag that will automatically parse a url string.
      function parseUrl(url) {
        if(!url) { return null; }
        var l = document.createElement("a");
        l.href = url;
        return l;
      }

      function getDomainName(url) {
        if(!url) { return null; }
        //Remove the www. part.
        var hostname = parseUrl(url).hostname.toLowerCase();
        var www = 'www.', start, end;

        //Extract the www.part;
        if((start = hostname.indexOf(www)) !== -1) {
          hostname = hostname.substring(start + www.length);
        }

        //Extract the high level domain.
        if((end = hostname.lastIndexOf('.')) !== -1) {
          hostname = hostname.substring(0, end);
        }
        
        return hostname;
      }

      //Fisher–Yates shuffle
      function shuffleArray(array) {
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      }

      return {
        shuffleArray: shuffleArray,
        isValidUrl: isValidUrl,
        parseUrl: parseUrl,
        getDomainName: getDomainName
      }
    }])
} ());
