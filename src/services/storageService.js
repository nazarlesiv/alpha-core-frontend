/**
 * Created by Nazar Lesiv on 8/11/2015.
 */
(function() {
  angular.module('ap.services').factory('storageService',
    ['$window',
      function ($window) {
        var _local = $window.localStorage;
        var _session = $window.sessionStorage;

        return {
          local: {
            add: function(key, value) {
              _local.setItem(key, angular.toJson(value))
            },
            get: function(key) {
              var value = _local.getItem(key);
              if(value) {
                value = angular.fromJson(value);
              }
              return value;
            },
            remove: function(key) {
              _local.removeItem(key)
            }
          },
          session: {
            add: function(key, value) {
              _session.setItem(key, angular.toJson(value))
            },
            get: function(key) {
              var value = _session.getItem(key);
              if(value) {
                value = angular.fromJson(value);
              }
              return value;
            },
            remove: function(key) {
              _session.removeItem(key)
            }
          }
        }


      }])
}());
