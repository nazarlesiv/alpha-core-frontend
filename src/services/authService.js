/**
 * Created by t430s on 7/10/2015.
 */
/*
 * Authentication and Authorization service.
 */

(function () {
  angular.module('ap.services').factory('authService',
    ['ajaxService', '$rootScope', '$location', '$log', 'storageService', '$q',
      function (ajaxService, $rootScope, $location, $log, storageService, $q) {

        //registry model to store private variables.
        var registry = {};

        var api = {
          account: function () {
            return registry.account || {};
          },
          user: function () {

            $log.debug('registry', registry);

            if (registry.account && registry.account.user) {
              return Array.isArray(registry.account.user) && registry.account.user.length > 0 ?
                registry.account.user[0] : registry.account.user;
            }
          },
          login: function (data) {

            //check if the ulr and data are provided.
            if (!data.url || typeof data.url !== 'string') {
              $log.debug('authService.user.login() - Invalid "url" Object')
              return $q.when(false);
            }
            if (!data.data || typeof data.data !== 'object') {
              $log.debug('authService.user.login() - Invalid "data" Object');
              return $q.when(false);
            }

            //ajax call to the server and get user data.
            return ajaxService.post(data.url, data.data)
              .then(function (res) {

                $log.debug('authService.user.login()', res);

                //Todo: make sure a valid account object is returned. Currently checking for an account _id.
                //Check for the existance for the login time.
                if (res && res.status === 200 && res.data && res.data._id) {
                  registry.account = res.data;

                  //store the account object in session storage.
                  if (storageService) {
                    storageService.session.add('account', registry.account);
                  }
                  //emit an even on the root scope for a successful login.
                  $rootScope.$emit('login:success');

                  return true;
                }
              })
              .catch(function (err) {
                return false;
                console.log(err);
              });
          },
          logout: function () {
            //ajax call to the server.
            $log.debug('authService.user.logout()')

            ajaxService.post('/account/logout').then(function (res) {
              console.log('res', res);
              if (res && res.status === 200) {
                //destroy the registry.account object.
                delete registry.account

                if (storageService) {
                  storageService.session.remove('account');
                }

                //emit an even on the root scope for a successful login.
                $rootScope.$emit('logout:success');
                return res;
              }

              return false;
            }).catch(function (err) {
              return err;
              console.log('failed to logout', err);
            });
          },
          isAuthenticated: function () {
            /************************** check if account is authorized and logged in ***************************/
            //this will need alot of attention to make sure the frontend does not indicate the account being
            //looged in while the backend session is not set.

            var result;

            //check if the account is in the registry or local storage.
            if (registry.account && registry.account) {
              result = true;
              //return $q.when(true);
            } else if (registry.account = storageService.session.get('account')) {
              result = true;
              //return $q.when(true);
            } else {
              result = false;
              //return $q.when(false);
            }

            return $q.when(result);

            //Todo Issue and ajax call to check the current loggedIn status.

          },
          can: function (ac, target) {
            //Check the permission the current user has on the target object.
            //ac: add,edit,delete,read
            if (!registry.account) {
              return false;
            }

            //todo - this is temporary, this will need to be updated for permissions on specific models.
            //Check if logged in.

            //If the user is an admin they can do everything.
            if (this.isAdmin()) {
              return true;
            }

            if (this.isModerator()) {
              return true;
            }
          },
          isAdmin: function () {
            //If the user is an admin they can do everything.
            if (registry.account && registry.account.permission &&
              registry.account.permission.name && registry.account.permission.name === 'administrator') {
              return true;
            }
          },
          isModerator: function () {
            if (registry.account && registry.account.permission &&
              registry.account.permission.name && registry.account.permission.name === 'moderator') {
              return true;
            }
          }
        }
        return api;
      }])
}());
