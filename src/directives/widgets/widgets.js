/**
 * Created by Nazar on 11/21/15.
 */

(function (angular) {
//root template url.

  //todo: Find a better way to package the html templates with the widgets.
  var basepath = '/public/alpha-core-frontend/src/directives/widgets/templates/';

  var module = angular.module('ap.widgets');

  module.directive('fileModel', ['$parse', function ($parse) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        var model = $parse(attrs.fileModel);
        var modelSetter = model.assign;

        element.bind('change', function(){
            scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
            });
        });
      }
    };
  }]);

  module.directive('apSelect', [ function() {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: basepath + 'ap-select.html',
      scope: {
        onrefresh: '=?onRefresh',
        onremove:'=?onRemove',
        onselect:'=?onSelect',
        confirmremove: '=?confirmRemove',
        confirmselect: '=?confirmSelect',   //If confirmSelect is set to true, the user will need to pass in true to the onselect callback to insert the item.
        items: '=items',
        delay: '=?delay',
        transform: '=?transform',
        tagging: '=?tagging',
        taggingLabel: '=?taggingLabel'
      },
      transclude: {
        'option' : 'apSelectOption',
        'match': '?apSelectMatch'
      },
      controller: ['$scope', '$timeout', '$log', function ($scope, $timeout, $log) {
        console.log('apTag$scope', $scope);
        var self = this;

        $scope.query = '';

        $scope.items = Array.isArray($scope.items) ? $scope.items: [] ;
        $scope.choices = [];

        $scope.delay = !isNaN($scope.delay) ? parseInt($scope.delay) : 100;

        $scope._select = function(item, index) {
          $scope.choices = [];
          $scope.query = '';

          if($scope.onselect) {
            // If confirm-remove is set to true, only remove an item if the supplied callback returned true.
            return $scope.onselect(item, index).then(function(res) {
              if($scope.confirmselect === true && res === true) {
                addItem(item);
              }
            });
          }
          addItem(item);

        };

        function addItem(item) {
          $scope.items.push(item);
        }

        $scope._keyup = function(e) {
          // Validate the key, If enter, select the first item in the list.
          if(e.keyCode === 13) {
            // Select the first option in the list.
            if($scope.choices.length > 0) {
              return $scope._select($scope.choices[0], 0);
            }
          }

          return refresh($scope.query);
        }

        $scope._remove = function(item, index) {
          // Call the onremove function if supplied.
          if($scope.onremove) {
            // If confirm-remove is set to true, only remove an item if the supplied callback returned true.
            return $scope.onremove(item, index).then(function(res) {
              if($scope.confirmremove === true && res === true) {
                removeItem(index);
              }
            });
          }

          removeItem(index);
        };

        function removeItem(index) {
          $scope.items.splice(index, 1);
        }

        $scope._focus = function(e) {
          return refresh();
        }
        $scope._blur = function(e) {
          $scope.query = '';
          $scope.choices = [];
        }

        function refresh(query) {
          $scope.query = query;

          if($scope.onrefresh) {
            $scope.onrefresh(query).then(function(response) {
              console.log('onrefresh', response);
              // Check that the response is a valid array.
              if(!response.length) {
                response = [];
              }

              // If tagging is enabled, add the value to the top of the array.
              if($scope.tagging) {
                response = process(response);
              }
              $scope.choices = response;
            });
          }
        }

        function process(items) {
          var trimRegex = '/^\s+|\s+$/gm';

          /* Check if query is empty, if so, return the items. */
          if(!$scope.query) { return items; }

          var item = transform($scope.query);

          /* Process the 'item.value' string before comparing it. */
          // Lower Case the value.
          var processed = $scope.query.toLowerCase();

          /* Trim off spaces. */
          processed = processed.replace(trimRegex,'');

          /* Loop over all items in the list and compared them to the new item. */
          for(var i = 0; i < items.length; ++i) {
            if(items[i].name.toLowerCase().replace(trimRegex,'') === processed) {
              /* If a Match was found, move the value to the front of the items array */
              items.unshift(items.splice(i, 1)[0]);

              /* Return the modified 'items' */
              return items;
            }
          }

          items.unshift(item);
          return items;
        }

        function transform(item) {
          if($scope.transform) {
            return $scope.transform($scope.query);
          } else {
            // Default transform.
            return {
              name: $scope.query,
              newTag: true
            };
          }
        }
      }],
      link: function (scope, element, attributes) {

      }
    }
  }]);

  module.directive('apSelectOption', [function(){
    return {
      restrict: 'EA',
      replace: true,
      transclude: true,
      templateUrl: basepath + 'ap-select-option.html',
      controller: ['$scope', '$timeout', '$log', function ($scope, $timeout, $log) {
        $scope._select = $scope.$parent._select;

        console.log('options', $scope);

      }]
    }
  }]);

  module.directive('apSelectMatch', [function(){
    return {
      restrict: 'EA',
      replace: true,
      transclude: true,
      templateUrl: basepath + 'ap-select-match.html',
      controller: ['$scope', '$timeout', '$log', function ($scope, $timeout, $log) {
        $scope._remove = $scope.$parent._remove;
        console.log('match', $scope);
      }]
    }
  }]);

  module.directive('apTicker', [function () {
    return {
      restrict: 'EA',
      replace: true,
      templateUrl: basepath + 'ap-ticker.html',
      scope: {
        items: '=items'

      },
      controller: ['$scope', '$interval', function ($scope, $interval) {
        //Set current active.
        var stopTimer;
        $scope.activeMsg = 0;

        $scope.setVisible = function (index) {
          if (index == $scope.activeMsg) {
            return ("1");
          } else {
            return ("0");
          }
        };

        $scope.refresh = function () {
          $scope.$apply(function () {
              $scope.activeMsg++;

              if ($scope.activeMsg >= $scope.items.length) {
                $scope.activeMsg = 0;
              }
            }
          );
        };

        stopTimer = setInterval($scope.refresh, 4000);
      }],
      link: function (scope, element, attributes) {

      }
    }
  }]);

  module.directive('apFooter', [function () {
    return {
      restrict: 'EA',
      replace: true,
      template: "<ng-include src='src'></ng-include>",
      scope: {
        src: '=template',
        config: '=config'
      },
      controller: ['$scope', '$location', function ($scope, $location) {
        //todo: add the logic to control when to show the footer based on the config object.
        $scope.show = $location.path() !== '/';

      }],
      link: function (scope, element, attributes) {

      }
    }

  }]);

  module.directive('apCarousel', ['$window', function ($window) {

    return {
      restrict: 'EA',
      templateUrl: basepath + 'carousel.html',
      replace: true,
      scope: {
        interval: '@interval',
        noWrap: '@noWrap',
        images: '=?images'
      },
      controller: ['$scope', function ($scope) {
        $scope.interval = $scope.interval || 5000;
        $scope.noWrap = $scope.noWrap || false;
        var slides = $scope.slides = [];
        $scope.addSlide = function (item) {
          slides.push(item);
        };
        if (Array.isArray($scope.images)) {
          $scope.images.forEach(function (val, i) {
            $scope.addSlide({
              image: val
            })
          })
        }
      }],
      link: function (scope, element, attributes) {
      }
    }
  }]);

  module.directive('apNavMenu', [function () {

    return {
      restrict: 'EA',
      replace: true,
      templateUrl: basepath + 'nav-menu.html',
      scope: {
        items: '=items',
        config: '=config',
      },
      controller: [
        '$scope',
        '$location',
        '$rootScope',
        'authService',
        'ajaxService',
        function ($scope,
                  $location,
                  $rootScope,
                  authService,
                  ajaxService) {

          $scope.submit = function (e, item) {
            //Collapse the menu
            $scope.navCollapsed = true;
            //Parse the Path into the right format (if the #is present, remove it.)
            console.log('item', item);
          };

          $scope.loggedIn = authService.isAuthenticated().then(function (val) {
            $scope.loggedIn = val;
          });

          $rootScope.$on('login:success', function () {
            authService.isAuthenticated().then(function (val) {
              $scope.loggedIn = val;
            })
          });

          $rootScope.$on('logout:success', function () {
            authService.isAuthenticated().then(function (val) {
              $scope.loggedIn = val;
            })
          });

          $scope.logout = function (e) {
            authService.logout();
          }
        }],
      link: function (scope, elem, attr) {
        scope.$watch(attr.items, function (newVal, oldVal) {

        });
      }
    }
  }]);

  module.directive('apGallery', [
    function () {
      return {
        restrict: 'AE',
        replace: true,
        templateUrl: basepath + 'ap-gallery.html',
        scope: {
          onselect: '=?onselect'
        },
        controller: [
          '$scope',
          'apGallery',
          function ($scope,
                    apGallery) {
            $scope.currentCategory = null;
            $scope.currentImage = null;
            $scope.categories = [];
            $scope.images = [];

            //Flag that determines if the Large Image div is visible.
            $scope.showImage = false;

            $scope.selected = function (e, item) {
              e.preventDefault();
              $scope.currentCategory = item;
              if ($scope.onselect) {
                $scope.onselect(item);
              }
            };

            $scope.selectImage = function (e, item) {
              e.preventDefault();
              $scope.currentImage = item;
              $scope.showImage = true;
            }

            //decorate the apGallery api object.
            apGallery.on = function (evt, handler) {
              var e = 'on' + evt;
              $scope[e] = handler;
            }
            apGallery.setCategories = function (items) {
              $scope.categories = items;
            };
            apGallery.getCategories = function () {
              return $scope.categories;
            };
            apGallery.setCurrentCategory = function (val) {
              $scope.currentCategory = val;
            };
            apGallery.getCurrentCategory = function () {
              return $scope.currentCategory;
            };
            apGallery.addImages = function (items) {
              $scope.images = items;
            };
            apGallery.getImages = function () {
              return $scope.images;
            }

            if (apGallery.onReady) {
              apGallery.onReady();
            }
          }
        ],
        link: function (scope, elem, attr) {

        }
      }
    }
  ]).factory('apGallery', [function () {
    //ap gallery directive will decorate the object.
    return {};
  }])

  module.directive('fixOnScroll', ['$window', function ($window) {
    return {
      restrict: 'A',
      scope: {
        applyClass: '@applyClass',
        offsetTop: '=?offsetTop',
        offsetLeft: '=?offsetLeft',
        offsetRight: '=?offsetRight',
        minMediaSize: '=?minMediaSize',
        callback: '=?callback'
      },
      link: function (scope, element, attrs) {

        //check for jQuery requirements.
        if (!jQuery && !$) {
          console.log('directive: fix-on-scroll requires jQuery');
          return;
        }

        scope.offsetTop = !isNaN(scope.offsetTop) ? parseInt(scope.offsetTop, 10) : 0;

        var win = $(window);
        var ele = $(element);
        var parent = ele.parent();

        //save the element origin style rules.
        var oldStyle = ele.css([
          'position',
          'top',
          'width',
          'height']);

        function scrollHandler(e) {
          var divTop = parent.offset().top;
          var windowTop = $(window).scrollTop();

          if (windowTop + scope.offsetTop > divTop) {

            if (scope.applyClass && typeof scope.applyClass === 'string') {
              $(ele).addClass(scope.applyClass);
            } else {
              //Todo: save old style and apply absolute position to the element.
              ele.css('position', 'fixed');
              ele.css('top', scope.offsetTop);
              ele.css('width', oldStyle.width || '');
              ele.css('z-index', '2');
              ele.css('background-color', oldStyle['background-color'] || 'white');
            }
          } else {
            if (scope.applyClass && typeof scope.applyClass === 'string') {
              $(ele).removeClass(scope.applyClass);
            } else {
              //Todo: apply the origin style to the element.
              ele.css('position', oldStyle.position || 'relative');
              ele.css('top', oldStyle.top || 'auto');
              ele.css('width', oldStyle.width || '');
              ele.css('z-index', oldStyle['z-index'] || '');
              ele.css('background-color', oldStyle['background-color'] || 'none');
            }
          }
        }

        function resizeHandler(e) {
          if (scope.minMediaSize && !isNaN(scope.minMediaSize)) {
            if (win.width() >= scope.minMediaSize) {
              $window.addEventListener('scroll', scrollHandler, false);
              //Call the scroll handler to update the element based on current
              //item location.
              scrollHandler();
            } else {
              $window.removeEventListener('scroll', scrollHandler, false);
              if (scope.applyClass && typeof scope.applyClass === 'string') {
                $(ele).removeClass(scope.applyClass);
              }
            }
          }
        }

        /*
         * Check if a minMediaSize was supplied.
         * If so, check that screen size a greather then or equal to the supplied value.
         */
        if (scope.minMediaSize && !isNaN(scope.minMediaSize)) {
          if (win.width() >= scope.minMediaSize) {
            $window.addEventListener('scroll', scrollHandler, false);
          }
        } else {
          $window.addEventListener('scroll', scrollHandler, false);
        }

        //Attach the resize handler to the resize event.
        $window.addEventListener('resize', resizeHandler, false);

        //Set the scroll handler to the callback.
        scope.callback = scrollHandler;
      }
    }
  }])

}(angular));
