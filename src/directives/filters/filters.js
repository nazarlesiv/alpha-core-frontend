/**
 * Created by Nazar on 11/21/15.
 */

(function (angular) {
  var module = angular.module('ap.filters');

  module.filter('propsFilter', function() {
    return function(items, props) {
      var out = [];

      if (angular.isArray(items)) {
        var keys = Object.keys(props);
          
        items.forEach(function(item) {
          var itemMatches = false;

          for (var i = 0; i < keys.length; i++) {
            var prop = keys[i];
            var text = props[prop].toLowerCase();
            if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
              itemMatches = true;
              break;
            }
          }

          if (itemMatches) {
            out.push(item);
          }
        });
      } else {
        // Let the output be the input untouched
        out = items;
      }

      return out;
    };
  });

  module.filter('trustAsHtml',
  ['$sce',
  function($sce) {
    return function(input) {
      if(!input) { return ''; }
      return $sce.trustAsHtml(input);
    }
  }]);

  module.filter('phone', function () {
    return function (tel) {
      if (!tel) { return ''; }

      var value = tel.toString().trim().replace(/^\+/, '');

      if (value.match(/[^0-9]/)) {
        return tel;
      }

      var country, city, number;

      switch (value.length) {
        case 10: // +1PPP####### -> C (PPP) ###-####
          country = 1;
          city = value.slice(0, 3);
          number = value.slice(3);
          break;

        case 11: // +CPPP####### -> CCC (PP) ###-####
          country = value[0];
          city = value.slice(1, 4);
          number = value.slice(4);
          break;

        case 12: // +CCCPP####### -> CCC (PP) ###-####
          country = value.slice(0, 3);
          city = value.slice(3, 5);
          number = value.slice(5);
          break;

        default:
          return tel;
      }

      if (country == 1) {
        country = "";
      }

      number = number.slice(0, 3) + '-' + number.slice(3);

      return (country + " (" + city + ") " + number).trim();
    };
  });

  module.filter('cut', function () {
    return function (value, wordwise, max, tail) {
      if (!value) return '';

      max = parseInt(max, 10);
      if (!max) return value;
      if (value.length <= max) return value;

      value = value.substr(0, max);
      if (wordwise) {
        var lastspace = value.lastIndexOf(' ');
        if (lastspace != -1) {
          value = value.substr(0, lastspace);
        }
      }

      return value + (tail || ' …');
    };
  })
    .filter('pad', function () {
      return function (value, side, filler, max) {
        if (!value || !value.length || value.length === 0 ||
          isNaN(max) || max <= value.length) { return value; }
        //Convert to string incase of number.
        var str = "" + value;

        //Calculat the required padding.
        var total = max - value.length;
        var padding = "";
        //Todo: check the filler string because if its a long string it will need to be truncated.
        for (var i = 0; i < total.length; i++) {
          padding += filler;
        }

        switch (side) {
          case 'l':
          case 'left':
            output = padding + value;
            break;
          case 'r':
          case 'right':
            output = value + padding;
            break;

        }
        return output;
      }
    });
} (angular));
