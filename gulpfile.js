/**
 * Created by admin on 3/16/16.
 */
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');

var name = 'alpha-core-frontend';
var output = '';
var js = [
  'src/**/main.js',
  'src/**/*.js',
];

var css = [
  'src/**/*.css',
];


gulp.task('concat', function() {
  output = name + '.js';
  return gulp.src(source)
    .pipe(concat(output))
    .pipe(gulp.dest('dist'));
});

gulp.task('buildjs', function() {
  output = name + '.min.js';
  return gulp.src(js)
    .pipe(concat(output))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});
gulp.task('buildcss', function() {
  output = name + '.min.css';
  return gulp.src(css)
    .pipe(concat(output))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('dist'));
});
gulp.task('build', ['buildjs', 'buildcss']);
/*
gulp.task('build', function() {
  output = name + '.min.js';
  return gulp.src(source)
    .pipe(concat(output))
    .pipe(uglify())
    .pipe(gulp.dest('dist'));
});
*/

gulp.task('watch', function() {
  //gulp.watch('css/**/*.less', ['less']);
  gulp.watch('src/**/*.js', ['build']);
});
